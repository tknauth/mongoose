/*
 * Copyright (c) 2014 Cesanta Software Limited
 * All rights reserved
 */

#include "mongoose.h"

static sig_atomic_t s_signal_received = 0;
static const char *s_http_port = "8000";
static struct mg_serve_http_opts s_http_server_opts;


static struct mg_connection* sqlite;
static const char *sqlite_addr = "127.0.0.1:8001";
static const char *sqlite_pref = "/api/v1";

static void signal_handler(int sig_num) {
  signal(sig_num, signal_handler);  // Reinstantiate signal handler
  s_signal_received = sig_num;
}

static int is_websocket(const struct mg_connection *nc) {
  return nc->flags & NSF_IS_WEBSOCKET;
}

static void broadcast(struct mg_connection *nc, const char *msg, size_t len) {
  struct mg_connection *c;
  char buf[500];

  snprintf(buf, sizeof(buf), "%p %.*s", nc, (int) len, msg);
  for (c = mg_next(nc->mgr, NULL); c != NULL; c = mg_next(nc->mgr, c)) {
      if (c->flags & NSF_USER_1)
          continue;
    mg_send_websocket_frame(c, WEBSOCKET_OP_TEXT, buf, strlen(buf));
  }
}

static void ev_handler(struct mg_connection *nc, int ev, void *ev_data) {
  struct http_message *hm = (struct http_message *) ev_data;
  struct websocket_message *wm = (struct websocket_message *) ev_data;

  switch (ev) {
    case NS_HTTP_REQUEST:
      /* Usual HTTP request - serve static files */
      mg_serve_http(nc, hm, s_http_server_opts);
      nc->flags |= NSF_SEND_AND_CLOSE;
      break;
    case NS_WEBSOCKET_HANDSHAKE_DONE:
      /* New websocket connection. Tell everybody. */
      broadcast(nc, "joined", 6);
      break;
    case NS_WEBSOCKET_FRAME: {
      char addr[100];
      memset(addr, 0, 100);
      snprintf(addr, 100,  "%s/%s", sqlite_pref, "msg"); 
      char* d = malloc(wm->size+1);
      memcpy(d, wm->data, wm->size);
      d[wm->size] = '\0';
      mg_printf(sqlite, "PUT %s HTTP/1.0\r\nHost: %s\r\nAccept: */*\r\nContent-Length: %d\r\nContent-Type: application/x-www-form-urlencoded\r\n\r\nvalue=%s",
                addr, sqlite_addr, wm->size+6, d);


      /* New websocket message. Tell everybody. */
      broadcast(nc, (char *) wm->data, wm->size);
      free(d);
      break;
                             }
    case NS_CLOSE:
      /* Disconnect. Tell everybody. */
      if (is_websocket(nc)) {
        broadcast(nc, "left", 4);
      }
      break;
    default:
      break;
  }
}

static void sqlite_handler(struct mg_connection *nc, int ev, void *ev_data) {
    int connect_status;

    switch (ev) {
        case NS_CONNECT:
            connect_status = * (int *) ev_data;
            if (connect_status == 0) {
                printf("sqlite connected \n");

                // retrieve history
                

            } else  {
                printf("connect() error: %s\n", strerror(connect_status));
            }
            break;
    }
}

int main(void) {
  struct mg_mgr mgr;
  struct mg_mgr mgr_sq;
  struct mg_connection *nc;


  signal(SIGTERM, signal_handler);
  signal(SIGINT, signal_handler);

  mg_mgr_init(&mgr, NULL);
  mg_mgr_init(&mgr_sq, NULL);

  nc = mg_bind(&mgr, s_http_port, ev_handler);
  s_http_server_opts.document_root = ".";
  mg_set_protocol_http_websocket(nc);
  mg_set_ssl(nc, "c.pem", NULL);

  if ((sqlite =  mg_connect(&mgr, sqlite_addr, sqlite_handler)) != NULL)
  mg_set_protocol_http_websocket(sqlite);
  mg_set_ssl(sqlite, "c.pem", NULL);
  sqlite->flags |= NSF_USER_1;

  printf("Started on port %s\n", s_http_port);
  while (s_signal_received == 0) {
    mg_mgr_poll(&mgr, 200);
  }
  mg_mgr_free(&mgr);

  return 0;
}
